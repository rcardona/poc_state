values_hash = {}
values_hash['!'] = '-- select from list --'
hosts_inventory = $evm.vmdb('Host').all

hosts_inventory.each do |this_host|
    values_hash[this_host.name] = this_host.name
end

list_values = {
   'sort_by'    => :value,
   'data_type'  => :string,
   'required'   => true,
   'values'     => values_hash
}
list_values.each { |key, value| $evm.object[key] = value }

exit MIQ_OK