# There are several ways to test/debug objects in the rails console, here it is recommended 
# to open two console prompts at the CFME appliance, where the first cosole would be used 
# for running the rails console itself, so called irb (interactive Ruby), and the second 
# console will be used to edit the scripts that will be tested in irb. The idea is to run  
# both consoles side to side in order to query/test the scripts.

# In the following site it will be found very useful documentation about existing CFME objects:
# http://manageiq.org/docs/reference/euwe/doc-Methods_Available_for_Automation/miq/#classifications-classification

# Most used objects
:Host  => refers to hypervisors
:Vm => refers to managed VM instances
:Ems => refers to Externa Management Systems (AWS, RHEV, Openstack, vCenter, Ansible Tower, etc)
:Switch => refers to virtualized switches
:Lan = > refers to vLans
:User => refers to CFME local users
:Miq_group => refers to CFME local user groups
:Cloud_network => refers to cloud networks
:Classifications => refers to the tag categories classification
:Ems_cluster => refers to the External Management clusters
:Ems_folder => refers to the External Management folder
:Volume => refers to storage volumes 
....
...
..
.



# Console session 1. It is possible to load objects to the irb and you could evaluate them in the form of $OBJECT.methods or $OBJECT.attributes.

$ vmdb

$ bin/rails c

irb(main):001:0> $evm = MiqAeMethodService::MiqAeService.new(MiqAeEngine::MiqAeWorkspaceRuntime.new)

# Console session 2

$ touch /tmp/access_evm.rb

$ vi /tmp/access_evm.rb


# Here are some examples of what could be the content of this Ruby scripts


# Example 1: Print a list of existing VMs objects
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
vms_inventory = $evm.vmdb(:Vm).all

vms_inventory.each do |this_vm|
   values_hash[this_vm.id] = this_vm.name
end

values_hash.each do |key, value|
    puts "VM Id =" + key.to_s + " :: " + "VM Name =" + value
end
________________________________________________________________________________


# Example 2: Print a list of existing VMs objects that run on VMWare
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
vms_inventory = $evm.vmdb(:Vm).all

vms_inventory.each do |this_vm|
  next if this_vm.type != "ManageIQ::Providers::Vmware::InfraManager::Vm"
    values_hash[this_vm.id] = this_vm.name
end

values_hash.each do |key, value|
  puts "VM Id =" + key.to_s + " :: " + "VM Name =" + value
end
________________________________________________________________________________


# Example 3: Print a list of existing External Management Systems (EMS)
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
ems_inventory = $evm.vmdb(:Ems).all

ems_inventory.each do |this_ems|
   values_hash[this_ems.id] = this_ems.name
end

values_hash.each do |key, value|
   puts "EMS Name: " + value + "  :::  " + "EMS ID: " + key.to_s
end
________________________________________________________________________________


# Example 4: Print a list of VLans specifically for VMWare
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
hosts_inventory = $evm.vmdb(:Host).all

hosts_inventory.each do |this_host|
  next if this_host.type != "ManageIQ::Providers::Vmware::InfraManager::HostEsx"
    this_host.lans.each do |this_host_lans|
      values_hash[this_host_lans.id] = this_host_lans.name
    end
end   

values_hash.each do |key, value|
   puts "LANs Name: " + value + "  :::  " + "LANs ID: " + key.to_s
end
________________________________________________________________________________


# Example 5: Print a list of ESX Hosts
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
hosts_inventory = $evm.vmdb('Host').all

hosts_inventory.each do |this_host|
  next if this_host.type != "ManageIQ::Providers::Vmware::InfraManager::HostEsx"
    values_hash[this_host.id] = this_host.name
end

values_hash.each do |key, value|
    puts "ESX Host Name: " + value + "  :::  " + "ESX Host ID: " + key.to_s
end
________________________________________________________________________________


# Example 6: Print a list of vSwitches
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
hosts_inventory = $evm.vmdb('Host').all

hosts_inventory.each do |this_host|
  next if this_host.type != "ManageIQ::Providers::Vmware::InfraManager::HostEsx"
    this_host.switches(:item).each do |this_host_switch|
        values_hash[this_host_switch.id] = this_host_switch.name
    end
end

values_hash.each do |key, value|
    puts "vSwitch Name: " + value + "  :::  " + "vSwitch ID: " + key.to_s
end
________________________________________________________________________________


# Example 7: Print a list of vSwitches discriminate by Host
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
hosts_inventory = $evm.vmdb('Host').all

hosts_inventory.each do |this_host|
  next if this_host.type != "ManageIQ::Providers::Vmware::InfraManager::HostEsx"
    this_host.switches(:item).each do |this_host_switch|
        values_hash[this_host_switch.id] = this_host.name.to_s + " - " +  this_host_switch.name.to_s
    end
end

values_hash.each do |key, value|
    puts "Host - vSwitch: " + value + "  :::  " + "vSwitch ID: " + key.to_s
end
________________________________________________________________________________


# Example 8: Print a list of Datecenters (So VMWare folders)
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
vmware_datacenter_inventory = $evm.vmdb(:Datacenter).all

vmware_datacenter_inventory.each do |this_datacenter|
  values_hash[this_datacenter.id] = this_datacenter.name
end

values_hash.each do |key, value|
    puts "Datacenter Name: " + value + "  :::  " + "Datacenter ID: " + key.to_s
end
________________________________________________________________________________


# Example 9: Print VMs and list in which datacenter they are hosted.
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
vms_inventory = $evm.vmdb(:Vm).all

vms_inventory.each do |this_vm|
  next if  this_vm.ems_folder.nil?
    values_hash[this_vm.name] = this_vm.datacenter.name
end

values_hash.each do |key, value|
    puts "VM Name: " + key + "  :::  " + "Datacenter Name: " + value
end
________________________________________________________________________________


# Example 10: Print Existing tag categories and current tags names.
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
key_count = 0

tag_categories_inventory = $evm.vmdb(:Classification).all

tag_categories_inventory.each do |tag_categories|
  tag_categories.entries.each do |tag_categories_names|
    values_hash[key_count] = " Tag Category :: Entry " + " =>  " + tag_categories.name + " :: " + tag_categories_names.name
    key_count += 1
  end
end

values_hash.each do |key, value|
    puts value
end
________________________________________________________________________________


# Example 11: Print Existing reports.
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'

reports_inventory = $evm.vmdb('miq_report').all

reports_inventory.each do |this_report|
  values_hash[this_report.id] = this_report.name
end

values_hash.each do |key, value|
    puts "Report ID: " + key.to_s + " ::: " +  "Report Name: " + value
end
________________________________________________________________________________


# Example 12: Print all vCenter hosted VM and their tags
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = []
vms_inventory = $evm.vmdb(:Vm).all


vms_inventory.each do |this_vm|
  next if this_vm.type != "ManageIQ::Providers::Vmware::InfraManager::Vm"
    next if this_vm.ems_blue_folder.nil?
      values_hash[this_vm.name] = this_vm.tags
end

values_hash.each do |key, value|
  value.each do |this_vm_tag|
    next if this_vm_tag.nil?
      puts "VM  Name=" + key  + " :: " + "VM Tags =" + this_vm_tag
  end
end
________________________________________________________________________________

# Example 13: Print all Miq Groups
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
miq_group_inventory = $evm.vmdb(:Miq_group).all

miq_group_inventory.each do |this_miq_group|
    values_hash[this_miq_group.id] = this_miq_group.description
end

values_hash.each do |key, value|
  puts "MIQ Group Id =" + key.to_s + " :: " + "Group Name =" + value
end
________________________________________________________________________________

# Example 14: Print all Tenants
________________________________________________________________________________
#!/usr/bin/ruby

values_hash = {}
values_hash['!'] = '-- select from list --'
tenant_inventory = $evm.vmdb(:Tenant).all

tenant_inventory.each do |tenant|
    values_hash[tenant.id] = tenant.name
end

values_hash.each do |key, value|
  puts "Tenant Id =" + key.to_s + " :: " + "Tenant Name =" + value
end

________________________________________________________________________________
# Example 15: Print a list of all vms in the vmdb
________________________________________________________________________________
#!/usr/bin/ruby
begin
  values_hash = {}
  values_hash['!'] = '-- select from list --'
  vms_inventory = $evm.vmdb(:vm).all

  vms_inventory.each do |this_vms_inventory|
    values_hash[this_vms_inventory.name] = this_vms_inventory.name
  end
  
    list_values = {
    'sort_by'    => :value,
    'data_type'  => :string,
    'required'   => true,
    'values'     => values_hash
  }
  list_values.each { |key, value| $evm.object[key] = value }

 rescue => err
  $evm.log(:error, "[#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_STOP
  
end
________________________________________________________________________________

# Example of how to output the hast values at the CFME

EXAMPLE FOR LISTING

begin
  values_hash = {}
  values_hash['!'] = '-- select from list --'
  ems_inventory = $evm.vmdb('ems').all

  ems_inventory.each do |this_ems_inventory|
    values_hash[this_vms_inventory.name] = this_vms_inventory.name
  end
  
    list_values = {
    'sort_by'    => :value,
    'data_type'  => :string,
    'required'   => true,
    'values'     => values_hash
  }
  list_values.each { |key, value| $evm.object[key] = value }

 rescue => err
  $evm.log(:error, "[#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_STOP
  
end


________________________________________________________________________________

# Listing a string only


if $evm.root['vm'].vendor.downcase == 'amazon'
   status = "Valid for this VM type"
 else
   status = 'Invalid for this VM type'
end

list_values = {
    'required'   => true,
    'protected'  => false,
    'read_only'  => true,
    'value'      => status,
}
  
list_values.each do |key, value|
    $evm.object[key] = value
end

________________________________________________________________________________

pay attention to this

${/#public_ip}

________________________________________________________________________________



