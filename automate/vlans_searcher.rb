values_hash = {}
values_hash['!'] = '-- select from list --'
hosts_inventory = $evm.vmdb(:Host).all
myesxhost = $evm.root['dialog_esxdroplist']
myswitch = $evm.root['dialog_vswitchdroplist']

hosts_inventory.each do |this_host|
  next if this_host.name != myesxhost
    this_host.switches.each do |this_host_switch|
      next if this_host_switch.name != myswitch
        this_host_switch.lans(:item).each do |this_host_switch_lans|
          values_hash[this_host_switch_lans.name] = this_host_switch_lans.name
        end
    end
end

list_values = {
   'sort_by'    => :value,
   'data_type'  => :string,
   'required'   => true,
   'values'     => values_hash
}
list_values.each { |key, value| $evm.object[key] = value }

exit MIQ_OK