
generated_string = ""; 8.times{generated_string  << (65 + rand(25)).chr}

generated_hostname = "prod-" + generated_string.downcase

list_values = {
   'required'   => true,
   'protected'  => false,
   'read_only'  => true,
   'value'     => generated_hostname,
}
list_values.each { |key, value| $evm.object[key] = value }

exit MIQ_OK